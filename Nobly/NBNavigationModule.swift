//
//  NBNavigationModule.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Dragos Resetnic. All rights reserved.
//

import UIKit

public class NBNavigationModule {
        
    static let shared = NBNavigationModule()
    
    var navigationController = UINavigationController()
    var appWindow: UIWindow? = nil
    
        init() {
        showResultScreen()
    }
    
    func showResultScreen(){
        let vc = ResultViewController()
        let _ = ResultModuleInitializer(resultViewController: vc)
        
        self.navigationController.pushViewController(vc, animated: false)
    }
    
    func showMatrixScreenWith(matrixInfo: MatrixInfo){
        let vc = MatrixViewController()
        let _ = MatrixModuleInitializer(matrixViewController: vc, matrixInfo: matrixInfo)
        self.navigationController.pushViewController(vc, animated: false)        
    }
}
