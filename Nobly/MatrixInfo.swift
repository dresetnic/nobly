//
//  MatrixInfo.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Dragos Resetnic. All rights reserved.
//

import Foundation

enum MatrixState:Int {
    case new,
    existing
}

enum MatrixType: Int{
    case simple,
    result
}


class MatrixInfo {
    var matrix:Matrix
    var exponent: Int
    var state: MatrixState = .new
    var type: MatrixType = .simple
    
    init(matrix: Matrix, exponent: Int) {
        self.matrix = matrix
        self.exponent = exponent
    }
}
