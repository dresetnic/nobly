//
//  CalculatorBrain.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Dragos Resetnic. All rights reserved.
//

import Foundation

public class NBCalculatorBrain {
    
    static let shared = NBCalculatorBrain()
    
    var allMatrices:[MatrixInfo] = []
    
    var result: MatrixInfo!
    
    var matricesSquareSize = 1
    
    func insertNewMatrix(matrixInfo: MatrixInfo){
        matrixInfo.state = .existing
        allMatrices.append(matrixInfo)
        recalculate()
    }
    
    func removeMatrixAt(idx: Int)-> Bool{
        if allMatrices.count > idx {
            allMatrices.removeLast()
            allMatrices.remove(at: idx)
            return true
        }
        return false
    }
    
    func pushTestData(){
        matricesSquareSize = 3
        
        let a = MatrixInfo(matrix: Matrix([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0 ,9.0]]), exponent: 3)
        let b = MatrixInfo(matrix: Matrix([[1, 1, 0], [0, 1, 1], [1, 0 ,1]]), exponent: 3)
        let c = MatrixInfo(matrix: Matrix([[1, -2, -3], [-1, -1, -1], [2, 2, -1]]), exponent: 3)
    
        insertNewMatrix(matrixInfo: a)
        insertNewMatrix(matrixInfo: b)
        insertNewMatrix(matrixInfo: c)
    }
    
    func newMatrix()->MatrixInfo{
        MatrixInfo(matrix: Matrix.zeros(size: (matricesSquareSize, matricesSquareSize)), exponent: 1)
    }
    
    func recalculate(){
        self.allMatrices = self.allMatrices.filter({ $0.state == .existing})
        calculateResult()
    }
    
    func calculateResult(){
        var result: Matrix = Matrix.zeros(size: (matricesSquareSize, matricesSquareSize))
        for matrixInfo in allMatrices{
            let squareRes = matrixInfo.matrix ^ matrixInfo.exponent
            result = result + squareRes
        }
        
        let resultMatrix = MatrixInfo(matrix: result, exponent: 1)
        resultMatrix.type = .result
        allMatrices.append(resultMatrix)
    }
    
    func cleanAll(){
        self.allMatrices.removeAll()
    }
}


