//
//  MatrixNBMatrixPresenter.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

class MatrixPresenter: MatrixModuleInput, MatrixViewOutput, MatrixInteractorOutput {
    
    weak var view: MatrixViewInput!
    var interactor: MatrixInteractorInput!
    var router: MatrixRouterInput!
    
    var oldVersion: MatrixInfo!
    
    var matrixInfo: MatrixInfo! {
        didSet {
            if matrixInfo.state == .existing {
                oldVersion = MatrixInfo(matrix: matrixInfo.matrix, exponent: matrixInfo.exponent)
            }
        }
    }
    
    func viewIsReady() {
        self.view.setExponentialTextField(value: matrixInfo.exponent)
    }
    
    func numberOfItemsInSection()->Int{
        return matrixInfo.matrix.count
    }
    
    func textFor(indexPath: IndexPath) -> String{
        return String(matrixInfo.matrix.grid[indexPath.row])
    }
    
    func changed(number: Double, forIndex index: Int){
        matrixInfo.matrix.grid[index] = number
    }
    
    func changedExponential(value: Int){
        matrixInfo.exponent = value
    }
    
    func touchedSaveButton(){
        if matrixInfo.state == .new {
            NBCalculatorBrain.shared.insertNewMatrix(matrixInfo: matrixInfo)
        }
        NBCalculatorBrain.shared.recalculate()
        self.router.popViewController()
    }
    
    func touchedCancel(){
        if matrixInfo.state == .existing {
            matrixInfo.matrix = oldVersion.matrix
            matrixInfo.exponent = oldVersion.exponent
        }
        NBCalculatorBrain.shared.recalculate()
        self.router.popViewController()
    }
}
