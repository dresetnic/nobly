//
//  MatrixNBMatrixViewController.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

class MatrixViewController: UIViewController, MatrixViewInput {
    
    var output: MatrixViewOutput!
    
    var exponentialTextField: UITextField = UITextField()
    
    var collectionView:UICollectionView? = nil
    
    var lastFocusedTextField = UITextField()
    
    let exponentialTextFieldTag = Int.max
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        setupInitialState()
    }
    
    
    // MARK: MatrixViewInput
    func setupInitialState() {
        createCollectionView()
        addSubviews()
        setupConstraints()
        setupSubviews()
    }
    
    func addSubviews(){
        let childSubviews: [UIView] = [collectionView!, exponentialTextField]
        
        for child in childSubviews {
            self.view.addSubview(child)
        }
    }
    
    func setupConstraints(){
        
        exponentialTextField.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(50)
        }
        
        collectionView?.snp.makeConstraints{ (make) in
            make.top.equalTo(exponentialTextField.snp.bottom).offset(50)
            make.left.right.bottom.equalToSuperview()
        }
    }
    
    func setupSubviews(){
        self.view.backgroundColor = .white
        
        showDoneButton()
        showCancelButton()
        
        exponentialTextField.borderStyle = .bezel
        exponentialTextField.keyboardType = .numberPad
        exponentialTextField.tag = exponentialTextFieldTag
        exponentialTextField.delegate = self
    }
    
    func setExponentialTextField(value: Int){
        exponentialTextField.text = String(value)
    }
    
    func showCancelButton(){
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(touchedCancel))
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    @objc func touchedCancel(){
        lastFocusedTextField.resignFirstResponder()
        self.output.touchedCancel()
    }
    
    func showDoneButton(){
        let addButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(touchedSave))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    
    @objc func touchedSave(){
        lastFocusedTextField.resignFirstResponder()
        self.output.touchedSaveButton()
    }
    
    func createCollectionView(){
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.sectionInset = UIEdgeInsets(top: 1, left: 5, bottom: 1, right: 5)
        let space = 0.0 as CGFloat
        flowLayout.minimumInteritemSpacing = space
        flowLayout.minimumLineSpacing = 0 as CGFloat
        flowLayout.itemSize = CGSize(width: 100, height:  55)
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.keyboardDismissMode = .onDrag
        collectionView?.register(NBCollectionViewMatrixCell.self, forCellWithReuseIdentifier: NBCollectionViewMatrixCell.reuseIdf)
        collectionView?.backgroundColor = .white
    }
}

extension MatrixViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.output.numberOfItemsInSection()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NBCollectionViewMatrixCell.reuseIdf, for: indexPath) as! NBCollectionViewMatrixCell
        
        cell.inputTextField.tag = indexPath.row
        cell.inputTextField.delegate = self
        cell.inputTextField.text = self.output.textFor(indexPath: indexPath)
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

extension MatrixViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lastFocusedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let row = textField.tag
        
        if let number = Double(textField.text ?? "0"){
            if row == exponentialTextFieldTag {
                self.output.changedExponential(value: Int(number))
            } else {
                self.output.changed(number: number, forIndex: row)
            }
        }
    }
}
