//
//  NBCollectionViewMatrixCell.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Dragos Resetnic. All rights reserved.
//

import UIKit

class NBCollectionViewMatrixCell: UICollectionViewCell {
    
    let inputTextField = UITextField()

    static let reuseIdf = "NBCollectionViewMatrixCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(){
        
        self.contentView.addSubview(inputTextField)
        
        inputTextField.snp.makeConstraints { (make) in
            make.height.equalTo(45)
            make.width.equalToSuperview()
            make.center.equalToSuperview()
        }
        
        inputTextField.placeholder = "value"
        inputTextField.keyboardType = .numberPad
    }
}
