//
//  MatrixNBMatrixViewInput.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

protocol MatrixViewInput: class {

    /**
        @author Dragos Resetnic
        Setup initial state of the view
    */

    func setupInitialState()
    func setExponentialTextField(value: Int)
}
