//
//  MatrixNBMatrixViewOutput.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

protocol MatrixViewOutput {

    /**
        @author Dragos Resetnic
        Notify presenter that view is ready
    */

    func viewIsReady()
    
    func numberOfItemsInSection()->Int
    
    func textFor(indexPath: IndexPath) -> String
    
    func changed(number: Double, forIndex index: Int)
    
    func changedExponential(value: Int)
    
    func touchedSaveButton()
    func touchedCancel()
}
