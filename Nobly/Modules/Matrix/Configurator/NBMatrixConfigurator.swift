//
//  NBMatrixNBMatrixConfigurator.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

class MatrixModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController, matrixInfo: MatrixInfo) {

        if let viewController = viewInput as? MatrixViewController {
            configure(viewController: viewController, matrixInfo: matrixInfo)
        }
    }

    private func configure(viewController: MatrixViewController, matrixInfo: MatrixInfo) {

        let router = MatrixRouter()

        let presenter = MatrixPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.matrixInfo = matrixInfo

        let interactor = MatrixInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }
}
