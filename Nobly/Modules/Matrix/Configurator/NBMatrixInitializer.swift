//
//  NBMatrixNBMatrixInitializer.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

class MatrixModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var matrixViewController: MatrixViewController!

    convenience init(matrixViewController: MatrixViewController, matrixInfo: MatrixInfo) {
        self.init()
        let configurator = MatrixModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: matrixViewController, matrixInfo: matrixInfo)
    }
}
