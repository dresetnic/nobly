//
//  MatrixNBMatrixRouter.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

class MatrixRouter: MatrixRouterInput {
    func popViewController(){
        NBNavigationModule.shared.navigationController.popViewController(animated: true)
    }
}
