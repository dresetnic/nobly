//
//  ResultNBResultPresenter.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

class ResultPresenter: ResultModuleInput, ResultViewOutput, ResultInteractorOutput {

    weak var view: ResultViewInput!
    var interactor: ResultInteractorInput!
    var router: ResultRouterInput!
    
    var pickedMatrixSize = false
    
    func viewIsReady() {
        NBCalculatorBrain.shared.pushTestData()
        self.view.reloadTableView()
    }
    
    func numberOfMatrices()->Int{
        return NBCalculatorBrain.shared.allMatrices.count//matrices.count
    }
    
    func matrixInfoForRow(_ row: Int)->MatrixInfo{
        return NBCalculatorBrain.shared.allMatrices[row]
    }

    func selectedMatrixAt(_ idx: Int){
        print(idx)
        
        self.router.showMatrixWith(matrixInfo: NBCalculatorBrain.shared.allMatrices[idx])
    }
    
    func viewWillAppear(){
        NBCalculatorBrain.shared.recalculate()
        self.view.reloadTableView()
    }
    
    func touchedAddButton(){
    
        if NBCalculatorBrain.shared.allMatrices.count == 0 && !pickedMatrixSize {
            self.view.showMatrixSizePicker()
            pickedMatrixSize = true
        } else  {
            showNewMatrixScreen()
        }
    }
    
    func touchedClearButton(){
        NBCalculatorBrain.shared.cleanAll()
        self.view.reloadTableView()
        pickedMatrixSize = false
    }
    
    func showNewMatrixScreen(){
        let newMatrix = NBCalculatorBrain.shared.newMatrix()
        self.router.showMatrixWith(matrixInfo: newMatrix)
    }
}
