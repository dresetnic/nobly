//
//  ResultNBResultRouter.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

class ResultRouter: ResultRouterInput {
    func showMatrixWith(matrixInfo: MatrixInfo){
        NBNavigationModule.shared.showMatrixScreenWith(matrixInfo: matrixInfo)
    }
}
