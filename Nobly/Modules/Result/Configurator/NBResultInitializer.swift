//
//  NBResultNBResultInitializer.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

class ResultModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var resultViewController: ResultViewController!

    override func awakeFromNib() {

        let configurator = ResultModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: resultViewController)
    }

	    convenience init(resultViewController: ResultViewController) {
        self.init()
        let configurator = ResultModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: resultViewController)
    }


}
