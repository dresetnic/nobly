//
//  NBResultNBResultConfigurator.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

class ResultModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ResultViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ResultViewController) {

        let router = ResultRouter()

        let presenter = ResultPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ResultInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
