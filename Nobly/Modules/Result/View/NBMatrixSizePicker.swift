//
//  NBMatrixSizePicker.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Dragos Resetnic. All rights reserved.
//

import UIKit

class NBMatrixSizePicker: UIView {
    
    let transparentBackground = UIView()
    
    let whiteContainer = UIView()
    
    let titleLabel = UILabel()
    let cancelButton = UIButton()
    let doneButton = UIButton()
    let pickerView = UIPickerView()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isExclusiveTouch = true
        addSubviews()
        setupConstraints()
        setupView()
    }
    
    convenience init(){
        self.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubviews()
        setupConstraints()
        setupView()
    }
    
    func addSubviews(){
        self.addSubview(transparentBackground)
        self.addSubview(whiteContainer)
        
        let childSubviews: [UIView] = [pickerView, titleLabel, cancelButton, doneButton]
        
        for child in childSubviews {
            whiteContainer.addSubview(child)
        }
    }
    
    func setupConstraints(){
        
        transparentBackground.snp.makeConstraints { (ConstraintMaker) in
            ConstraintMaker.size.equalToSuperview()
            ConstraintMaker.center.equalToSuperview()
        }
        
        whiteContainer.snp.makeConstraints { (ConstraintMaker) in
            ConstraintMaker.center.equalToSuperview()
            ConstraintMaker.right.equalToSuperview().inset(15)
            ConstraintMaker.left.equalToSuperview().inset(15)
            ConstraintMaker.height.equalToSuperview().inset(15)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(pickerView)
            make.height.equalTo(40)
            make.bottom.equalTo(pickerView.snp.top).offset(10)
        }
        
        pickerView.snp.makeConstraints { (make) in
                  make.center.equalToSuperview()
                  make.width.equalToSuperview()
                  make.height.equalTo(300)
        }
        
        cancelButton.snp.makeConstraints { (make) in
            make.height.equalTo(55)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(15)
            make.width.equalToSuperview().inset(15)
        }

        doneButton.snp.makeConstraints { (make) in
            make.height.centerX.width.equalTo(cancelButton)
            make.bottom.equalTo(cancelButton.snp.top).offset(-15)
        }
    }
    
    func setupView() {
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColor(.blue, for: .normal)
        doneButton.setTitle("Done", for: .normal)
        doneButton.layer.backgroundColor = UIColor.blue.cgColor
        doneButton.setTitleColor(.white, for: .normal)
        
        self.backgroundColor = .clear
        transparentBackground.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.whiteContainer.clipsToBounds = true
        self.whiteContainer.backgroundColor = .white
        self.whiteContainer.layer.cornerRadius = 4
        
        
        titleLabel.text = "Choose matrix size"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
    }
}
