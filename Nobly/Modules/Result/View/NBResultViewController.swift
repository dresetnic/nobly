//
//  ResultNBResultViewController.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, ResultViewInput {
    
    var output: ResultViewOutput!
    
    var tableView = UITableView(frame: CGRect.zero)
    
    let matrixSizePickerView = NBMatrixSizePicker()
    
    var matrixSize = NBCalculatorBrain.shared.matricesSquareSize
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.output.viewWillAppear()
    }
    
    // MARK: ResultViewInput
    
    func setupInitialState() {
        addSubviews()
        setupConstraints()
        setupViews()
    }
    
    func addSubviews(){
        //        self.view.addSubview(navigationView)
        self.view.addSubview(tableView)
        //        self.view.addSubview(continueButton)
    }
    
    func setupConstraints(){
        
        self.tableView.snp.makeConstraints { (ConstraintMaker) in
            ConstraintMaker.center.size.equalToSuperview()
        }
    }
    
    func setupViews(){
        
        self.view.backgroundColor = .white
        
        self.tableView.separatorStyle = .none
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.keyboardDismissMode = .onDrag
        
        tableView.register(NBTableViewMatrixCell.self, forCellReuseIdentifier: NBTableViewMatrixCell.rIdf)
        tableView.register(NBTableViewMatrixResultCell.self, forCellReuseIdentifier: NBTableViewMatrixResultCell.rIdf)
        
        self.showAddButton()
        self.showClearButton()
    }
    
    func showMatrixSizePicker(){
        self.navigationController?.view.addSubview(matrixSizePickerView)
        
        matrixSizePickerView.snp.makeConstraints { (make) in
            make.size.equalToSuperview()
        }
        
        matrixSizePickerView.pickerView.delegate = self
        matrixSizePickerView.pickerView.dataSource = self
        
        matrixSizePickerView.doneButton.addTarget(self, action: #selector(pickedSize), for: .touchUpInside)
        matrixSizePickerView.cancelButton.addTarget(self, action: #selector(dissmissPickerView), for: .touchUpInside)

    }
    
    @objc func pickedSize(){
        NBCalculatorBrain.shared.matricesSquareSize = matrixSize
        dissmissPickerView()
    }
    
    @objc func dissmissPickerView(){
        if (matrixSizePickerView.superview != nil) {
            matrixSizePickerView.removeFromSuperview()
        }
    }
    
    func showAddButton(){
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(touchedAdd))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    
    @objc func touchedAdd(){
        self.output.touchedAddButton()
    }
    
    func showClearButton(){
        let clearButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(touchedClear))
        self.navigationItem.leftBarButtonItem = clearButton
    }
    
    
    @objc func touchedClear(){
        self.output.touchedClearButton()
    }
    
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.output.numberOfMatrices()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellInfo = self.output.matrixInfoForRow(indexPath.row)
        
        if indexPath.row == self.output.numberOfMatrices() - 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: NBTableViewMatrixResultCell.rIdf, for: indexPath) as! NBTableViewMatrixResultCell
            cell.configureWith(matrixInfo: cellInfo)
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: NBTableViewMatrixCell.rIdf, for: indexPath) as! NBTableViewMatrixCell
            cell.configureWith(matrixInfo: cellInfo)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.output.selectedMatrixAt(indexPath.row)
    }
    
}

extension ResultViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row + 1) x \(row + 1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        matrixSize = row + 1
    }
}
