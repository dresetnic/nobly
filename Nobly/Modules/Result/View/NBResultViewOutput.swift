//
//  ResultNBResultViewOutput.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

protocol ResultViewOutput {

    /**
        @author Dragos Resetnic
        Notify presenter that view is ready
    */

    func viewIsReady()
    func viewWillAppear()
    func numberOfMatrices()->Int
    func matrixInfoForRow(_ row: Int)->MatrixInfo
    func selectedMatrixAt(_ idx: Int)

    func touchedAddButton()
    func touchedClearButton()
    
}
