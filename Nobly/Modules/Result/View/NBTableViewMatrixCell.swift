//
//  NBTableViewMatrixCell.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Dragos Resetnic. All rights reserved.
//

import UIKit
import SnapKit

class NBTableViewMatrixCell: UITableViewCell {
    
    static let rIdf = "NBTableViewMatrixCell"
    
    let matrixDescription = UITextView()
    let expLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupCell(){
        addSubviews()
        setupConstraints()
        setupView()
    }
    
    func addSubviews(){
        let subviewsarray:[UIView] = [matrixDescription, expLabel]
        
        for view in subviewsarray {
            self.contentView.addSubview(view)
        }
    }
    
    func setupConstraints(){
        
        matrixDescription.snp.makeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(15)
        }
        
        expLabel.snp.makeConstraints { (make) in
            make.height.equalTo(55)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().inset(15)
        }
    }
    
    func setupView(){
        self.clipsToBounds = true
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        matrixDescription.isUserInteractionEnabled = false
    }
    
    func configureWith(matrixInfo: MatrixInfo){
        expLabel.text = "exp:\(matrixInfo.exponent)"
        matrixDescription.text = "\(matrixInfo.matrix)"
    }
}
