//
//  ResultNBResultViewInput.swift
//  Nobly
//
//  Created by Dragos Resetnic on 16/12/2019.
//  Copyright © 2019 Nobly. All rights reserved.
//

protocol ResultViewInput: class {

    /**
        @author Dragos Resetnic
        Setup initial state of the view
    */

    func setupInitialState()
    func reloadTableView()
    
    func showMatrixSizePicker()
}
