# Nobly Technical Test


Matrix Adder
You are required to build a program that can take at least 3 square matrices with exponents and
return a new matrix.
The input will be the square matrices and exponents.
The program you write will need to have the ability to allow the user to enter at least any 3
matrices (and their exponents) and give the user the new matrix in the output.
The program will need to sum the results of the exponents of the square matrices to create the
output matrix.
We will consider the tool based on its effectiveness and whether the best UI practices are used.
Considerations:
- Matrix are squares
- Minimum amount of inputted matrices: 3
- Needs to be as effective as possible
- Input and Output must be solved by the candidate
- Use best practices when possible
- Each exponent is only for the Matrix entered
- Due to the potential size of the matrices, you will need to take into consideration the
memory available and how you represent the input/output
Example
A = [ {1, 2, 3}, {4, 5, 6}, {7, 8 ,9} ]
B = [ {1, 1, 0}, {0, 1, 1}, {1, 0, 1} ]
C = [ {1, -2, -3}, {-1, -1, -1}, {2, 2, -1} ]
AExp = 3
BExp = 3
CExp = 3
A^ 3 = [ {468, 576, 684}, {1062, 1305, 1548}, {1656, 2034 ,2412} ]
B^ 3 = [ {2, 3, 3}, {3, 2, 3}, {3, 3, 2} ]
C^ 3 = [ {7, 16, 13}, {7, 13, 0}, {-8, -2, 21} ]
A^3 + B^3 + C^3 = [ {477, 595, 700}, {1072, 1320, 1551}, {1651, 2035, 2435} ]


# ios

## Prerequisities

- XCode 10 and command line tools
- [Homebrew](https://brew.sh)
- [Carthage package manager](https://github.com/Carthage/Carthage#installing-carthage)
- Navigate to the root directory of your project in Terminal.
- Run the following command "carthage update --platform iOS"


## References
 [Exponentiation_by_squaring](https://en.m.wikipedia.org/wiki/Exponentiation_by_squaring)
